#ifndef ACTIVE_OBJECT_H_
#define ACTIVE_OBJECT_H_

#include "circularfifo.h"
#include "activecallback.h"
#include <memory>


namespace ActiveObject
{
    // redicolusly small queue, to force "misses" and you to change queue
    static const int c_queueSize = 10000;
    
    typedef CircularFifo<Callback *, c_queueSize> msg_queue;
    
    class Active 
    {
        
        protected :
            //All messages are pushed into this queue 
            msg_queue mq;
            
            bool _done;
            
            
            // \pre the task existing
            // \post Active flag for can to destroy threads
            virtual void _doDone(){_done = true;}
            
            // \pre ActiveUnix is created
            // \post run method to another context
            static void* _runThread(void* args);
            
            
        private:
            
            // \pre None
            // \post Task is started
            void run();
            
        public:
            
            // \pre ActiveObject is running
            // \post Add callback to circular Fifo for to executed later
            bool send(Callback * msg_) 
            {
                return mq.push(msg_);
            } 
    };
}

#endif //ACTIVE_OBJECT_H_
