//
//  main.cpp
//  TestActiveObject
//
//  Created by jessy giacomoni on 27/02/2014.
//  Copyright (c) 2014 jessy giacomoni. All rights reserved.
//

#include <unistd.h>
#include "activeObject.h"
#include "displayConsole.h"
#include "activeUnix.h"

int main(int argc, const char * argv[])
{
	ActiveUnix * _activeObject = ActiveUnix::createActive();

    displayToConsole * m_display  =  new displayToConsole();
	printf("Begin \n");
    int intParam  = 42;
    _activeObject->send(ActiveObject::bind(m_display, &displayToConsole::displayConsoleWithMessage,intParam));

    for (int i=0; i<5;i++) 
    {
        printf("Main : continue \n");
        sleep(1);
    }
    printf("End \n");
}

