#ifndef ACTIVE_CALLBACK_H_
#define ACTIVE_CALLBACK_H_

#include <memory>

namespace ActiveObject {

  /// Baseclass for callbacks. Gives easy storage in a container
  // without exposign the template type
  struct Callback {
    virtual void operator()() = 0;
    virtual ~Callback(){};
  };


  template<typename T, typename Param>
  class ActiveCallback: public Callback {
   private:
     typedef void (T::*Func)(Param);

     T*    object;
     Func func;
     Param param;
  
   public:
     ActiveCallback(T* obj_, void (T::*f_)(Param), Param& p_)
		: object(obj_), func(f_), param(p_) { } 

    virtual ~ActiveCallback() {}
    virtual void operator()() { (object->*func)(param); }     
  };


    
  template<typename T>
  class ActiveCallback<T, void>: public Callback {
   private:
    typedef void (T::*Func)(); // same as: "void (T::*method)(void)"
     
     T*    object;
     Func func;  

   public:
     ActiveCallback ( T* obj_, Func f_): object(obj_), func(f_) {} 
         virtual ~ActiveCallback() {}
         virtual void operator()() { (object->*func)(); } 
      };


   template<typename T>
   Callback * bind (T* obj_, void (T::*f_)(void)) {
      Callback* func = new ActiveCallback<T, void>(obj_,f_);
      return func;
   }


   template<typename T, typename Param>
   Callback * bind (T* obj_, void(T::*f_)(Param), Param& p_)
	{
      Callback * func = new ActiveCallback<T, Param> (obj_,f_,p_);
      return func;
   }

} 
#endif

