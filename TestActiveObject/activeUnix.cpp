#include "activeUnix.h"
#include "activecallback.h"

using namespace ActiveObject;

//------------------------------------------------------------------------------------------------------------------
ActiveUnix::ActiveUnix(): m_thread(NULL){};

//------------------------------------------------------------------------------------------------------------------
ActiveUnix::~ActiveUnix()
{
    while(!send(bind(this, &ActiveUnix::_doDone)))
	{
        pthread_yield_np();
	}
    pthread_join(m_thread, NULL);
}

//------------------------------------------------------------------------------------------------------------------
void Active::run() 
{
    while (!_done) {
        if (mq.isEmpty()) 
        {
            pthread_yield_np(); // help switching if on same core
        }
        else 
        {
            Callback * msg;
            mq.pop(msg);
            (*msg)();
        }
    }
}

//------------------------------------------------------------------------------------------------------------------
void* ActiveUnix::_runThread(void* args) 
{
    Active::_runThread(args);
    pthread_exit(NULL);
}


//------------------------------------------------------------------------------------------------------------------
ActiveUnix* ActiveUnix::createActive()
{
    ActiveUnix * unix (new ActiveUnix());
    pthread_create(&(unix->m_thread), NULL, _runThread, unix);
    return unix;
}