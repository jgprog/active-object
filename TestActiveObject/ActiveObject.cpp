#include "activeObject.h"
#include <cassert>

//------------------------------------------------------------------------------------------------------------------
void* ActiveObject::Active::_runThread(void* args) 
{
    Active* pActive = static_cast<Active*>(args);
    assert(NULL != pActive);
    pActive->run();
    return NULL;
}