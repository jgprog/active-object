//
//  activeUnix.h
//  TestActiveObject
//
//  Created by jessy giacomoni on 28/02/2014.
//  Copyright (c) 2014 jessy giacomoni. All rights reserved.
//

#ifndef TestActiveObject_activeUnix_h
#define TestActiveObject_activeUnix_h


#include "activeObject.h"
#include <pthread.h> 

using namespace ActiveObject;

class ActiveUnix : public Active
{
    
    private :
        pthread_t m_thread;

        // \pre See parent
        // \post See parent
        virtual void _doDone(){Active::_doDone();}
        
        // \pre See parent
        // \post See parent
        static void* _runThread(void* args);
        
    public:
        
        explicit ActiveUnix();
        
        virtual ~ActiveUnix();
        
        // \pre None
        // \post create a task
        static ActiveUnix* createActive();

};


#endif
