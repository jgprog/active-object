//
//  displayToConsole.h
//  TestActiveObject
//
//  Created by jessy giacomoni on 27/02/2014.
//  Copyright (c) 2014 jessy giacomoni. All rights reserved.
//

#ifndef TestActiveObject_dispplayConsole_h
#define TestActiveObject_dispplayConsole_h

class displayToConsole 
{
    public:
        explicit displayToConsole() {}
        
        void displayConsole()
        {
            sleep(1);
            printf("This feature was run from another context ! \n");
        }
    
        void displayConsoleWithMessage( int  intParam)
        {
            sleep(1);
            printf("This feature was run from another context ! The integer is : %d \n", intParam);
        }
    
};
#endif
